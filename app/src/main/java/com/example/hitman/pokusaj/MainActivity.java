package com.example.hitman.pokusaj;

import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SeekBar;
import android.widget.TextView;
import android.graphics.*;
import android.widget.*;
import android.content.Intent;



public class MainActivity extends AppCompatActivity {

    //varijable za boju, početne i krajnje vrijednosti za seekbar, boolean zastavice za odabran/neodabran prozor
    private int crvena=0, zelena=0, plava=0;
    private int step=1, max=255, min=0;
    private int clicked_prvi_prozor=0,clicked_drugi_prozor=0,clicked_treci_prozor=0;
    private boolean focused_prvi_prozor=false, focused_drugi_prozor=false, focused_treci_prozor=false, uspjeh=false;

    //deklaracije i inicijalizacije bitmapa koje koriste objeki ImageView za crtanje linija koje će se prikazati na 3 prozora
    private  Bitmap shema_prozora_1=Bitmap.createBitmap(200,200,Bitmap.Config.ARGB_8888);               //bitmapa za prikazivanje na prvom prozoru
    private  Bitmap shema_prozora_2=Bitmap.createBitmap(200,200,Bitmap.Config.ARGB_8888);               //bitmapa za prikazivanje na drugom prozoru
    private  Bitmap shema_prozora_3=Bitmap.createBitmap(200,200,Bitmap.Config.ARGB_8888);               //bitmapa za prikazivanje na trecem prozoru
    private  Bitmap bitmap = Bitmap.createBitmap(200,200,Bitmap.Config.ARGB_8888);                      //bitmapa na kojoj će se prikazivati obojana kružnica

    //definiranje "kista" i platna
    private  Paint linije = new Paint();
    private  Paint brisanje_linija=new Paint();
    private  Paint za_preview_boja=new Paint();

    private  Canvas crte_prozora_1=new Canvas(shema_prozora_1);
    private  Canvas crte_prozora_2=new Canvas(shema_prozora_2);
    private  Canvas crte_prozora_3=new Canvas(shema_prozora_3);
    private  Canvas canvas = new Canvas(bitmap);

    //deklaracija SeekBar-ova, TextView-ova i ImageView-ova na kojima će se pokazivati ono što je nacrtano na bitmapu
    private SeekBar seekBarR, seekBarG, seekBarB;
    private TextView tekstR, tekstG, tekstB;
    private ImageView prvi_prozor, drugi_prozor, treci_prozor, prozor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        switch (getResources().getConfiguration().orientation){
            case Configuration.ORIENTATION_PORTRAIT:
                setContentView(R.layout.activity_main);
                break;
            case Configuration.ORIENTATION_LANDSCAPE:
                setContentView(R.layout.activity_main_land);
                break;
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        seekBarR=(SeekBar)findViewById(R.id.seekBar);
        seekBarG=(SeekBar)findViewById(R.id.seekBar2);
        seekBarB=(SeekBar)findViewById(R.id.seekBar3);

        tekstR=(TextView)findViewById(R.id.textView1);
        tekstG=(TextView)findViewById(R.id.textView2);
        tekstB=(TextView)findViewById(R.id.textView6);

        seekBarR.setMax((max-min)/step);
        seekBarG.setMax((max-min)/step);
        seekBarB.setMax((max-min)/step);

        tekstR.setText(String.valueOf(crvena));
        tekstG.setText(String.valueOf(zelena));
        tekstB.setText(String.valueOf(plava));

        prvi_prozor = (ImageView)findViewById(R.id.prvi_prozor);
        drugi_prozor = (ImageView)findViewById(R.id.drugi_prozor);
        treci_prozor = (ImageView)findViewById(R.id.treci_prozor);
        prozor = (ImageView)findViewById(R.id.prozor);

        //"kist" za crtanje crnih obrisa
        linije.setStrokeWidth(10);
        linije.setStyle(Paint.Style.FILL);
        linije.setColor(Color.BLACK);

        //"kist" za "brisanje" crnih linija bijelim kada se odznači prozor
        brisanje_linija.setStrokeWidth(10);
        brisanje_linija.setStyle(Paint.Style.FILL);
        brisanje_linija.setColor(Color.rgb(250,250,250));

        //"kist" za bojanje kružnice
        za_preview_boja.setColor(Color.rgb(crvena,zelena,plava));
        za_preview_boja.setStyle(Paint.Style.FILL_AND_STROKE);
        za_preview_boja.setFilterBitmap(false);

        centralneLinijeProzora(prvi_prozor,crte_prozora_1, shema_prozora_1, linije);
        centralneLinijeProzora(drugi_prozor,crte_prozora_2, shema_prozora_2, linije);
        centralneLinijeProzora(treci_prozor,crte_prozora_3, shema_prozora_3, linije);

        prvi_prozor.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {

                clicked_prvi_prozor++;
                if(clicked_prvi_prozor==1){
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),"Odabrali ste prvi prozor.", 1000);
                    snackbar.show();

                    obrubProzora(prvi_prozor,crte_prozora_1,shema_prozora_1,linije);
                    prvi_prozor.getDrawable().setColorFilter(Color.rgb(crvena,zelena,plava),PorterDuff.Mode.SRC_ATOP);
                    focused_prvi_prozor=true;
                }
                else if((clicked_prvi_prozor%2)==0) {
                    obrubProzora(prvi_prozor,crte_prozora_1,shema_prozora_1,brisanje_linija);
                    clicked_prvi_prozor=0;
                    focused_prvi_prozor=false;
                }
            }
        });

        drugi_prozor.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                clicked_drugi_prozor++;
                if(clicked_drugi_prozor==1){
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),"Odabrali ste drugi prozor.", 1000);
                    snackbar.show();
                    obrubProzora(drugi_prozor,crte_prozora_2,shema_prozora_2,linije);
                    drugi_prozor.getDrawable().setColorFilter(Color.rgb(crvena,zelena,plava),PorterDuff.Mode.SRC_ATOP);
                    focused_drugi_prozor=true;
                }
                else if((clicked_drugi_prozor%2)==0) {
                    obrubProzora(drugi_prozor,crte_prozora_2,shema_prozora_2,brisanje_linija);
                    clicked_drugi_prozor=0;
                    focused_drugi_prozor=false;
                }
            }
        });

        treci_prozor.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view){
                clicked_treci_prozor++;
                if(clicked_treci_prozor==1){
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),"Odabrali ste treći prozor.", 1000);
                    snackbar.show();
                    obrubProzora(treci_prozor,crte_prozora_3,shema_prozora_3,linije);
                    treci_prozor.getDrawable().setColorFilter(Color.rgb(crvena,zelena,plava),PorterDuff.Mode.SRC_ATOP);
                    focused_treci_prozor=true;
                }
                else if((clicked_treci_prozor%2)==0) {
                    obrubProzora(treci_prozor,crte_prozora_3,shema_prozora_3,brisanje_linija);
                    clicked_treci_prozor=0;
                    focused_treci_prozor=false;
                }
            }
        });


        seekBarR.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBarR, int progress, boolean fromUser) {
                crvena=min+(progress*step);
                tekstR.setText(String.valueOf(crvena));

                prozor.setColorFilter(Color.rgb(crvena,zelena,plava));
                seekBarR.getProgressDrawable().setColorFilter( Color.rgb(crvena,0,0) , PorterDuff.Mode.MULTIPLY);
                if(focused_prvi_prozor==true) {
                    prvi_prozor.getDrawable().setColorFilter(Color.rgb(crvena,zelena,plava),PorterDuff.Mode.SRC_ATOP);
                }
                if(focused_drugi_prozor==true){
                    drugi_prozor.getDrawable().setColorFilter(Color.rgb(crvena,zelena,plava),PorterDuff.Mode.SRC_ATOP);
                }
                if(focused_treci_prozor==true){
                    treci_prozor.getDrawable().setColorFilter(Color.rgb(crvena,zelena,plava),PorterDuff.Mode.SRC_ATOP);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBarR) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBarR) {

            }

        });


        seekBarG.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBarG, int progress, boolean fromUser) {
                zelena=min+(progress*step);
                tekstG.setText(String.valueOf(zelena));

                prozor.setColorFilter(Color.rgb(crvena,zelena,plava));
                seekBarG.getProgressDrawable().setColorFilter( Color.rgb(0,zelena,0) , PorterDuff.Mode.MULTIPLY);
                if(focused_prvi_prozor==true) {
                    prvi_prozor.getDrawable().setColorFilter(Color.rgb(crvena,zelena,plava),PorterDuff.Mode.SRC_ATOP);
                }
                if(focused_drugi_prozor==true){
                    drugi_prozor.getDrawable().setColorFilter(Color.rgb(crvena,zelena,plava),PorterDuff.Mode.SRC_ATOP);
                }
                if(focused_treci_prozor==true){
                    treci_prozor.getDrawable().setColorFilter(Color.rgb(crvena,zelena,plava),PorterDuff.Mode.SRC_ATOP);
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        seekBarB.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBarB, int progress, boolean fromUser) {
                plava=min+(progress*step);
                tekstB.setText(String.valueOf(plava));

                prozor.setColorFilter(Color.rgb(crvena,zelena,plava));
                seekBarB.getProgressDrawable().setColorFilter( Color.rgb(0,0,plava) , PorterDuff.Mode.MULTIPLY);

                if(focused_prvi_prozor==true) {
                    prvi_prozor.getDrawable().setColorFilter(Color.rgb(crvena,zelena,plava),PorterDuff.Mode.SRC_ATOP);
                }
                if(focused_drugi_prozor==true){
                    drugi_prozor.getDrawable().setColorFilter(Color.rgb(crvena,zelena,plava),PorterDuff.Mode.SRC_ATOP);
                }
                if(focused_treci_prozor==true){
                    treci_prozor.getDrawable().setColorFilter(Color.rgb(crvena,zelena,plava),PorterDuff.Mode.SRC_ATOP);
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });



        canvas.drawCircle(bitmap.getWidth()/2,bitmap.getHeight()/2,bitmap.getWidth()/2, za_preview_boja);
        prozor.setImageBitmap(bitmap);

        //tipka s kojom se potvrđuje slanje proizvonjih boja prozora na bazu podataka
        Button button_bojaj=(Button)findViewById(R.id.btn_bojaj);

        button_bojaj.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){

                String crvena_string=Integer.toString(crvena);
                String zelena_string=Integer.toString(zelena);
                String plava_string=Integer.toString(plava);

                if(focused_prvi_prozor){
                    try{

                            String prozor_string=Integer.toString(1);
                            new BackgroundUbacivanjeBoja().execute(crvena_string,zelena_string,plava_string,prozor_string);
                            uspjeh=true;
                    }
                    catch(Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),"PROBLEM!!!",Toast.LENGTH_SHORT).show();
                    }
                }

                if(focused_drugi_prozor){
                    try{
                        String prozor_string=Integer.toString(2);
                        new BackgroundUbacivanjeBoja().execute(crvena_string,zelena_string,plava_string,prozor_string);
                        uspjeh=true;
                    }
                    catch(Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),"PROBLEM!!!",Toast.LENGTH_SHORT).show();
                    }

                }

                if(focused_treci_prozor){
                    try{
                        String prozor_string=Integer.toString(3);
                        new BackgroundUbacivanjeBoja().execute(crvena_string,zelena_string,plava_string,prozor_string);
                        uspjeh=true;
                    }
                    catch(Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),"PROBLEM!!!",Toast.LENGTH_SHORT).show();
                    }

                }

                if(uspjeh) {
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),"Uspješno poslane boje.", 1500);
                    snackbar.show();
                }

                if(!focused_prvi_prozor && !focused_drugi_prozor && !focused_treci_prozor){
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),"Nijedan prozor nije odabaran.", 1500);
                    snackbar.show();
                }

                obrubProzora(prvi_prozor,crte_prozora_1,shema_prozora_1,brisanje_linija);
                obrubProzora(drugi_prozor,crte_prozora_2,shema_prozora_2,brisanje_linija);
                obrubProzora(treci_prozor,crte_prozora_3,shema_prozora_3,brisanje_linija);

                clicked_prvi_prozor=0;
                clicked_drugi_prozor=0;
                clicked_treci_prozor=0;

                focused_prvi_prozor=false;
                focused_drugi_prozor=false;
                focused_treci_prozor=false;
                uspjeh=false;

                seekBarR.setProgress(0);
                seekBarG.setProgress(0);
                seekBarB.setProgress(0);
            }
        });

    }

    void obrubProzora(ImageView prozor,Canvas canvas, Bitmap bitmap, Paint kist){
        canvas.drawLine(0,bitmap.getHeight(), 0,0,kist);                              //vertikalna linija prozora "lijevo"
        canvas.drawLine(bitmap.getWidth(),bitmap.getHeight(), bitmap.getWidth(),0,kist);           //vertikalna linija prozora "desno"
        canvas.drawLine(0,bitmap.getHeight(),bitmap.getWidth(),bitmap.getHeight(),kist);           //horizontalna linija prozora "gore"
        canvas.drawLine(0,0,bitmap.getWidth(),0,kist);                                //horizontalna linija prozora "dole"
        prozor.setImageBitmap(bitmap);
    }

    void centralneLinijeProzora(ImageView prozor,Canvas canvas, Bitmap bitmap, Paint kist){
        canvas.drawLine(5,bitmap.getHeight()/2, (bitmap.getWidth())-5,bitmap.getHeight()/2,linije);
        canvas.drawLine(bitmap.getWidth()/2,(bitmap.getHeight())-5,bitmap.getWidth()/2,5,linije);
        prozor.setImageBitmap(bitmap);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_tutorial) {
            startActivity(new Intent(MainActivity.this,tutorial.class));
            return true;
        }
        if(id==R.id.action_about) {
            startActivity(new Intent(MainActivity.this,about.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


}
