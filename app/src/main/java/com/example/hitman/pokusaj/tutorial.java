package com.example.hitman.pokusaj;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;
import static android.text.Html.FROM_HTML_MODE_LEGACY;

public class tutorial extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        TextView opis_aplikacije = (TextView) findViewById(R.id.tutorial_textView);
        String tekst="<br/>" + "<br/>" + "Dobro došli u aplikaciju za mijenjanje boja prozora.<br/>" +
                "<br/>" + "<br/>" +
                "Ovaj tekst služi kako bi Vam pomogli pri navigaciji i korištenju aplikacije. Generalno, postoje dva vrlo slična načina rada." + "<br/>" + "<br/>" + "<br/>" +
                "<b>1. način:</b> <br/>" + "<br/>" +
                "a) Odaberite prozore koje želite obojati." + "<br/>" +
                "b) Promijenite iznose klizača dok ne dođete do željene boje." + "<br/>" +
                "c) Odaberite <b>\"BOJAJ!\"</b>" + "<br/>" +
                "d) <i>Pogledajte na prozore!</i>" + "<br/>" + "<br/>" + "<br/>" +
                "<b>2.način:</b> <br/>" + "<br/>" +
                "a)Promijenite iznose klizača dok ne vidite željenu boju, a koju možete vidjeti u krugu pored tipke <b>BOJAJ!</b>" + "<br/>" +
                "b)Odaberite <b>\"BOJAJ!\"</b>" + "<br/>" +
                "c)<i>Pogledajte na prozore!</i>" + "<br/>" + "<br/>";
        opis_aplikacije.setText(Html.fromHtml(tekst,FROM_HTML_MODE_LEGACY));
    }


}
